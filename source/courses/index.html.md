---
layout: markdown_page
title: Courses
description: Learn about GitLab courses, including course listings and information for adding and uploading courses.
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Deprecation

We are deprecating this page.
As noted in [our handbook guidlines](/handbook/handbook-usage/#guidelines) we want to organize per function and result, not per format.
All video's on this page should be embedded in the relevants part of the handbook.



### Leadership (LDR) courses

- LDR 102 [Underperformance](https://www.youtube.com/watch?v=nRJHvzXwXBU&list=PLFGfElNsQthYYlad7vtTUt3wXKQUTsZWz&index=3) and [slide deck](https://docs.google.com/presentation/d/1kVbFPzuVpjXhHNG2po8-j3013RUEYN0jK4n8ecbCU9I/edit#slide=id.g1fa059f00f_0_0)
- LDR 104 [Promotions](https://www.youtube.com/watch?v=TNPLiYePJZ8&t=492s) and [slide deck](https://docs.google.com/presentation/d/1QvWSTO2NCfz8XBMwnUBElvx-d7zGIi3WIEl8L9DR4tg/edit#slide=id.g1fa059f00f_0_0)
- LDR 105 [How to Give Performance Review Feedback](https://youtu.be/VK8cA8nYcoY) and [slide deck](https://docs.google.com/presentation/d/1obMvPNicsQBB8_vQC1OaQnsNPLzKkoxvzxFoQCPjzuU/edit#slide=id.g1fa059f00f_0_0)
- LDR 106 [Decision Making](https://youtu.be/7RbVQwU69H0) and [slide deck](https://docs.google.com/presentation/d/1ua-EShTeT1PD23Yw-5C2OTnqy8x58nR--b6QeRKeToI/edit#slide=id.g1fa059f00f_0_0)
- LDR 107 [Diversity & Inclusion Pt1](https://youtu.be/5bCmjiK8h5w) and [slide deck](https://docs.google.com/presentation/d/1lUACwyEGJAjhAB6HAcgXIP3CjTKz4nBzeKwoOsKsWGA/edit#slide=id.g25803b2e16_0_97)
- LDR 108 [Coaching](https://www.youtube.com/watch?v=uMs0Gzf5Gkw) and [slide deck](https://docs.google.com/presentation/d/1zGSfMSJeCjFQN7UPdVxV7Lg-u5gbCDfVMjLsSG0dmlw/edit#slide=id.g2a1e9c2bfd_0_178)
- LDR 109 [Reference Checks](https://www.youtube.com/watch?v=8pdf_rRihcE) and [slide deck](https://docs.google.com/presentation/d/18UevZe1tpngZiaejcWEwuITYvvi7wEBch7yc26eGHHM/edit#slide=id.g156008d958_0_18)
- LDR 110 [Performance Management Feedback](https://www.youtube.com/watch?v=D6Ko3_pVGjw)and [slide deck](https://docs.google.com/presentation/d/1DKPTU2eskWXs8b8NG_4mkiLvu5oLIbYgoLwJCLmous8/edit#slide=id.g29a70c6c35_0_68)


### Sales (SLS) courses

 - SLS001 [How to Conduct an Executive Meeting](https://www.youtube.com/watch?v=PSGMyoEFuMY&feature=youtu.be)

### Customer Success (CST) courses

**Office Hours**

- CST101 [GitLab I2P In-depth with Mark P.](https://youtu.be/fzlDQ2j-jv8)

**Explainer Videos**

- CST201 [GitHub.com vs. GitLab](https://www.youtube.com/watch?v=ZdmDJFPNQuI&index=1&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST202 [Hosting Gitlab in the Cloud](https://www.youtube.com/watch?v=1OLCDtUkw0Q&index=2&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST203 [Integrating Atlassian and GitLab](https://www.youtube.com/watch?v=o7pnh9tY5LY&index=3&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST204 [GitLab Maintenance and Support](https://www.youtube.com/watch?v=X8jsj59b4vk&index=4&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST205 [We already use JIRA, so shouldn't we go with BitBucket?](https://www.youtube.com/watch?v=-JRab22h9Dg&index=5&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST206 [Traditional DevOps Dasiy Chain](https://www.youtube.com/watch?v=YHznYB275Mg)
- CST207 [Automate to Accelerate](https://www.youtube.com/watch?v=dvayJWwzfPY&t=465s)
- CST208 [Installing GitLab on GKE](https://www.youtube.com/watch?v=HLNNFS8b_aw&t=1s)
- CST209 [Connecting GitLab.com to your private cloud on GCP](https://www.youtube.com/watch?v=j6HRDquF0mQ)
- CST210 [Complete DevOps with GitLab](https://www.youtube.com/watch?v=68rGlAihKFw)


### Build (BLD) courses

- BLD001 [GitLab Pivotal Cloud Foundry Tile](https://youtu.be/oo2p6WtHhG4)
- BLD002 [GitLab Terraform Module](https://youtu.be/JbbKq0UrDec)
- BLD003 [Kubernetes](https://youtu.be/Po8vUvoiMYU)
- BLD004 [Omnibus](https://youtu.be/m89NHLhTMj4)
- BLD005 [PG HA](https://youtu.be/2Uz2piFLp7k)
- BLD006 [GitLab QA project](https://youtu.be/Ym159ATYN_g)

### Engineering (DEV) courses

- [DEV 101 - Contributing to Golang projects](/courses/dev-101)


### Infrastructure (INF) courses

- [INF 201 - Using Terraform to manage the GitLab.com infrastructure](/courses/inf-201)
